// Pins
int red = 8;
int yellow = 10;
int green = 12;

void setup() {
  // put your setup code here, to run once:
  // Set all pins as output.
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  // Turn on red led.
  digitalWrite(red, HIGH);
  // Wait 1 second.
  delay(1000);
  // Turn off red and turn on green.
  digitalWrite(red, LOW);
  digitalWrite(green, HIGH);
  // Wait 1 second.
  delay(1000);
  // Turn off green and turn on yellow.
  digitalWrite(green, LOW);
  digitalWrite(yellow, HIGH);
  // Wait 1 second.
  delay(1000);
  // Turn off yellow.
  digitalWrite(yellow, LOW);
}

