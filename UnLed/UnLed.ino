int pin = 13;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  // Encendre el led.
  digitalWrite(pin, HIGH);
  // Esperar 1 segon.
  delay(1000);
  // Apagar el led.
  digitalWrite(pin, LOW);
  // Esperar 2 segons.
  delay(2000);
}
